<?php
namespace Modules\Articles\Exceptions;

use Exception;

class ArticleNotFoundException extends Exception
{
}