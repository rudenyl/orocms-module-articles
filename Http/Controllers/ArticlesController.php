<?php 
namespace Modules\Articles\Http\Controllers;

use Gate;
use Modules\Articles\Repositories\ArticleRepository;
use OroCMS\Admin\Controllers\BaseController;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ArticlesController extends BaseController 
{
    use AuthorizesRequests;

    protected $route_prefix = 'articles';
    protected $view_prefix = 'articles';
    protected $theme = '';

    protected $repository;

    function __construct(ArticleRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $articles = $this->repository->active();

        return $this->view('index', compact('articles'));
    }

    public function show($slug)
    {
        $article = $this->repository->findActiveBy('slug', $slug);

        // check authorization
        $article->publishing->access && $this->authorize($article);
        
        $view = $this->view('article', compact('article'));

        # 
        # onAfterRenderItem
        #
        event('articles.onAfterRenderItem', $view);

        return $view;
    }
}
