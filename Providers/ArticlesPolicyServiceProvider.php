<?php
namespace Modules\Articles\Providers;

use Gate;
use Illuminate\Support\ServiceProvider;

class ArticlesPolicyServiceProvider extends ServiceProvider
{
	/**
	 * Boot the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		// define our policies here
		Gate::define('show-article', function($user, $article) {
			return $user->id == $article->user_id;
		});

		Gate::before(function ($user, $ability) {
			if ($user->is('admin')) {
				return true;
			}
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
	}
    
	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return [];
	}

}
