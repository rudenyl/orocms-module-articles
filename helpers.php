<?php
/**
 * Blade helpers
 */

Blade::directive('html', function($expression) {
    return "<?php echo html_entity_decode($expression); ?>";
});

Blade::directive('htmlsafe', function($expression) {
    return "<?php echo nl2br(htmlentities($expression)); ?>";
});
