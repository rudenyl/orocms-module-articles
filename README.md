# Overview
Articles module for [GitLab: OroCMS](https://gitlab.com/rudenyl/orocms-admin.git)


## Installation
Clone or copy to `/modules` directory on your root Laravel installation.
This will then appear under the `Settings --> Modules` tab in your admin panel.

Clone as follows:
```
$ cd modules
$ git clone https://gitlab.com/rudenyl/orocms-module-articles.git
```

_(Optional)_ To manually install, issue a **php artisan** command with:
```
$ php artisan admin:migration --module articles
```

All set :)

# Troubleshooting
```
Class 'Modules\Articles\Providers\ArticlesServiceProvider' not found
```
The above exception is due to the `/modules` path not properly loaded. In your app's `composer.json` file, make sure you have the following lines:
```
    ...
    "autoload": {
        "psr-4": {
            "App\\": "app/",
            ...
            "Modules\\": "modules/"  // <--- add this
        },
        ...
    },
    ...
```
